/**
 * @param {string} name Name of the Country
 * @param {number} count Population of the Country
 * @param {string} region The Country's region.
 */
const Country = function(name, count, region){
	if(typeof(region) !== "string"){
		throw new TypeError("Parameter region has to be a string");
	}
	if(typeof(name) !== "string"){
		throw new TypeError("Parameter name has to be a string");
	}
	if(typeof(count) !== "number"){
		throw new TypeError("Parameter number has to be a number");
	}
	if(count <= 0){
		throw new RangeError("Parameter number needs to be positive");
	}

	this.name = name;
	this.region = region;
	this.count = count;
}

/**
 * @type {Country[]}
 */
let countries = [ 
	new Country("China",		1439323776,"Asia"), 
	new Country("India",		1380004385,"Asia"), 
	new Country("United States", 331002651,"America"), 
	new Country("Indonesia",	 273523615,"Asia"), 
	new Country("Pakistan",		 220892340,"Asia"),
	new Country("Brazil",		 212559417,"America"),
	new Country("Russia",		 145996764,"Europe"),
	new Country("Germany",		  83871995,"Europe"),
	new Country("Turkey",		  84422510,"Europe"),
	new Country("France",		  65255278,"Europe"),
	new Country("United Kingdom", 67881733,"Europe"),
	new Country("Italy",		  60480665,"Europe"),
	new Country("Spain",		  46776338,"Europe"),
];



let app; 




document.addEventListener("DOMContentLoaded",onLoad,false);

function onLoad(){


	// Vue Component List Entry
	Vue.component("list-entry", {props: ["country"], template: "<tr><td>{{country.name}}</td><td>{{country.population}}</td><td>{{country.region}}</td></tr>"});

	// MaterialzeCSS Init
    var instances = M.AutoInit();
	// Vue Init
	app = new Vue({el:"#app", data: {
		countries: countries,
		lowerPopulationBound : 60_000_000,
		regions: {Europe:true, Asia:false, America:false},
		valueToAdd: {},
	}, methods: {
		addCountry: (vals)=>{
			try{
				console.info(vals.Name,vals.Count, vals.Region)
				app.countries = [...app.countries, new Country(vals.Name, Number(vals.Count), vals.Region)]; 
				vals.Name = vals.Count = vals.Region = "";
			}catch (e){
				M.toast({html:"ERR: Could not add Country. "+e.message})
			}
		}
	}});

	


}



